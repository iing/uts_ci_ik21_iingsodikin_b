<?php defined('BASEPATH') OR exit('No direct script access allowed');

class transaksi_model extends CI_Model
{
	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		
		$this->load->model("Menu_model");
		$this->load->model("Kasir_model");
		
		
	}
    //panggil nama table
    private $_table = "transaksi";
    

    public function tampilDatatransaksi()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDatatransaksi2()
	
	{
		$query = $this->db->query(
			"SELECT * from transaksi as ts
			INNER JOIN master_kasir AS mk ON ts.no_kasir = mk.no_kasir
			INNER JOIN master_menu AS mn ON ts.kode_menu = mn.kode_menu");
			$data = $query->result();

		return $query->result();
	}
	
	public function tampilDatatransaksi3()
	
	{
		$this->db->select('*');
		$this->db->order_by('id_transaksi', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	
     
	public function savetransaksi()
	{
		
       	$no_kasir           = $this->input->post('no_kasir');
        $kode_menu          = $this->input->post('kode_menu');
        $qty                = $this->input->post('qty');
		$harga         	 	= $this->Menu_model->cariHargaMenu($kode_menu);
		
		//$data['id_pemesanan']	= $id;
		$data['no_kasir']		= $no_kasir;
		$data['id_transaksi']	= $id_transaksi;
		$data['tgl_pembelian']	= date('Y-m-d');
		
		$data['kode_menu']		= $kode_menu;
		$data['qty']			= $qty;
		$data['harga']	= $qty * $harga;
			$this->db->insert($this->_table, $data);
	}

	
}