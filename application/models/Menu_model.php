<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Menu_model extends CI_Model
{
	//panggil nama table
	private $_table = "master_menu";
	
	public function tampilDatamenu()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDatamenu2()
	
	{
		$query = $this->db->query("SELECT * FROM master_menu WHERE flag = 1");
		return $query->result();
	}
	
	public function tampilDatamastermenu3()
	
	{
		$this->db->select('*');
		$this->db->order_by('kode_menu', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	
	{
		
		
		$data['kode_menu'] =$this->input->post('kode_menu');
		$data['nama_menu'] =$this->input->post('nama_menu');
		$data['harga'] =$this->input->post('harga');
		$data['kode_jenis'] =$this->input->post('kode_jenis');
		
		
		$data['flag'] =1;
		$this->db->insert($this->_table, $data);
	}
	
	public function update($kode_menu)
	
	{
		
		
		$data['nama_menu'] =$this->input->post('nama_menu');
		$data['harga'] =$this->input->post('harga');
		$data['kode_jenis'] =$this->input->post('kode_jenis');
		$data['flag'] =1;
		$this->db->where('kode_menu', $kode_menu);
		$this->db->update($this->_table, $data);

	}
	public function delete($kode_menu)
	
	{
		
		$this->db->where('kode_menu', $kode_menu);
	    $this->db->delete($this->_table);
		
	}
	public function cariHargaMenu($kode_menu)
	{
		$query	= $this->db->query("SELECT * FROM " . $this->_table . " 
		WHERE flag = 1 AND kode_menu = '$kode_menu'");
        $hasil = $query->result();

		foreach ($hasil as $data){
			$harganya = $data->harga;
		}
		
		return $harganya;
	}

}
