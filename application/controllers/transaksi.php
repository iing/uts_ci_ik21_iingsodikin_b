<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class transaksi extends CI_controller {

	
	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("transaksi_model");
		$this->load->model("Kasir_model");
		$this->load->model("Menu_model");
		
	}
	
	public function index()
	
	{
		$this->listtransaksi();
	}
	
	public function listtransaksi()
	
	{
		$data['data_transaksi'] = $this->transaksi_model->tampilDatatransaksi2();
		$this->load->view('home_transaksi', $data);
	}
	
	public function inputtransaksi()
	
	{    
		$data['data_kasir'] = $this->Kasir_model->tampilDataKasir();
		$data['data_menu'] = $this->Menu_model->tampilDatamenu();
		$data['data_transaksi'] = $this->transaksi_model->tampilDatatransaksi2();
	
		if (!empty($_REQUEST)) {
        $m_transaksi = $this->transaksi_model;
        @$m_transaksi->savetransaksi();    
       
            redirect("transaksi/listtransaksi", "refresh");
        }
        
		$this->load->view('input_transaksi', $data);
    }

	  
	
}
