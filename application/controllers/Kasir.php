<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Kasir extends CI_controller {

	
	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("Kasir_model");
		
	}
	
	public function index()
	
	{
		$this->listKasir();
	}
	
	public function listKasir()
	
	{
		$data['data_kasir'] = $this->Kasir_model->tampilDataKasir();
		$this->load->view('home_kasir', $data);
	}
	public function inputkasir()
	
	{
		$data['data_kasir'] = $this->Kasir_model->tampilDataKasir();
		if (!empty($_REQUEST)) {
			$m_kasir = $this->Kasir_model;
			$m_kasir->save();
			redirect("kasir/index", "refresh");
		}
			
		$this->load->view('input_kasir', $data);
	}
	public function detailKaryawan($nik)
	
	{
		$data['detail_kasir'] = $this->Kasir_model->detail($no_kasir);
		$this->load->view('detail_kasir', $data);
					
	}
	public function editkasir($no_kasir)
	
	{
		$data['detail_kasir'] = $this->Kasir_model->detail ($no_kasir) ;
			
		if(!empty($_REQUEST)){
			$m_kasir = $this->Kasir_model;
			$m_kasir->update($no_kasir);
			redirect("kasir/index", "refresh");
	}	
		$this->load->view('edit_kasir', $data);
	}

	public function deletekasir($no_kasir)
	{
		$m_kasir = $this->Kasir_model;
		$m_kasir->delete($no_kasir);
		redirect("kasir/index", "refresh");
	}
}
