<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Jenis extends CI_controller {

	
	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("Jenis_model");
		
	}
	
	public function index()
	
	{
		$this->listJenis();
	}
	
	public function listJenis()
	
	{
		$data['data_jenis'] = $this->Jenis_model->tampilDataJenis();
		$this->load->view('home_jenis', $data);
	}
	public function inputjenis()
	
	{
		$data['data_jenis'] = $this->Jenis_model->tampilDataJenis();
		if (!empty($_REQUEST)) {
			$m_jenis = $this->Jenis_model;
			$m_jenis->save();
			redirect("jenis/index", "refresh");
		}
			
		$this->load->view('input_jenis', $data);
	}
	public function detailJenis($kode_jenis)
	
	{
		$data['detail_jenis'] = $this->Jenis_model->detail($kode_jenis);
		$this->load->view('detail_jenis', $data);
					
	}
	public function editjenis($kode_jenis)
	
	{
		$data['detail_jenis'] = $this->Jenis_model->detail ($kode_jenis) ;
			
		if(!empty($_REQUEST)){
			$m_jenis = $this->Jenis_model;
			$m_jenis->update($kode_jenis);
			redirect("jenis/index", "refresh");
	}	
		$this->load->view('edit_jenis', $data);
	}

	public function deletejenis($kode_jenis)
	{
		$m_jenis = $this->Jenis_model;
		$m_jenis->delete($kode_jenis);
		redirect("jenis/index", "refresh");
	}
}
