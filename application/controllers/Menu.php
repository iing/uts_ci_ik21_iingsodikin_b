<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Menu extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("menu_model");
		$this->load->model("Jenis_model");
		
	}
	
	public function index()
	
	{
		$this->listmenu();
	}
	
	public function listmenu()
	
	{
		$data['data_menu'] = $this->menu_model->tampilDatamenu();
		$this->load->view('home_menu', $data);
	}
	public function inputmenu()
	
	{
		$data['data_jenis'] = $this->Jenis_model->tampilDataJenis();
		if (!empty($_REQUEST)) {
			$m_menu = $this->menu_model;
			$m_menu->save();
			redirect("menu/index", "refresh");
		}
			
		$this->load->view('input_menu', $data);
	}
	
	public function editmenu($kode_menu)
	
	{
		$data['data_jenis'] = $this->Jenis_model->tampilDataJenis();
		$data['data_menu'] = $this->menu_model->tampilDatamenu($kode_menu);
		if (!empty($_REQUEST)) {
			$m_menu = $this->menu_model;
			$m_menu->update($kode_menu);
			redirect("menu/index", "refresh");
		}
		$this->load->view('edit_menu', $data);
		
	}
	public function delete($kode_menu)
	
	{
			$m_menu = $this->menu_model;
			$m_menu->delete($kode_menu);
			redirect("menu/index", "refresh");
	}
}