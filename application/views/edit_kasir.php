<?php
foreach ($detail_kasir as $data) {
	$no_kasir  = $data->no_kasir;
	$nama_operator  = $data->nama_operator;
  $nik_operator  = $data->nik_operator;
	$jenis_kelamin  = $data->jenis_kelamin;
	
	$tgl_lahir  = $data->tgl_lahir;
	$telp  = $data->telp;
	$alamat  = $data->alamat;
}
     //pisah tanggal bualn tahun
	 $thn_pisah = substr($tgl_lahir, 0, 4);
	 $bln_pisah = substr($tgl_lahir, 5, 2);
	 $tgl_pisah = substr($tgl_lahir, 8, 2);
	?>
<table width="50%" height="78" border="1" align="center">
<tr>
 
  <td colspan="5" align="center" bgcolor="#33FFCC"><b>EDIT MASTER KASIR</td></b>
 
 
  
  </tr>
<form action="<?=base_url()?>kasir/editkasir/<?= $no_kasir; ?>" method="POST">
<table width="50%" border="0" cellspacing="0" cellpadding="5" bgcolor="#999999" align="center">
  <tr>
    <br />  
    <td>no kasir</td>
    <td>:</td>
    <td>
      <input value="<?= $no_kasir; ?>" type="text" name="no_kasir" id="no_kasir" readonly></td>
  </tr>
  <tr>
    <td>nama operator</td>
    <td>:</td>
    <td>
      <input value="<?= $nama_operator; ?>" type="text" name="nama_operator" id="nama_operator" maxlength="50"></td>
  </tr>
   <tr>
    <td>nik operator</td>
    <td>:</td>
    <td>
      <input value="<?= $nik_operator; ?>" type="text" name="nik_operator" id="nik_operator" maxlength="50"></td>
  </tr>
   <tr>
    <td height="35">jenis kelamin</td>
    <td>:</td>
    <td><select name="jenis_kelamin" id="jenis_kelamin">
       <?php
	if($jk == 'P'){
		$slc_p = 'selected';
		$slc_l = '';
	}else if($jk == 'L'){
		$slc_l = 'selected';
		$slc_p = '';
	}else{
		$slc_p = '';
		$slc_l = '';
		}
	?>
     <option <?=$slc_p;?> value="p">perempuan</option>
      <option  <?=$slc_l;?> value="l">laki-laki</option>
      
    </select>
    </td>
  </tr>

 
   <tr>
    <td>tanggal lahir</td>
    <td>:</td>
    <td><select name="tgl" id="tgl">
    <?php
	for($tgl=1;$tgl<=31;$tgl++){
		$select_tgl = ($tgl == $tgl_pisah) ? 'selected' : '';
		?>
        <option value="<?=$tgl;?>" <?=$select_tgl;?>><?=$tgl; ?></option>
        <?php
	}
	?>
    </select>
      <select name="bln" id="bln">
       <?php
	   $bln_n = array('januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember');
	for($bln=0;$bln<12;$bln++){
		$select_bln = ($bln == $bln_pisah) ? 'selected' : '';
		?>
       
        <option value="<?=$bln+1;?>" <?= $select_bln; ?>>
		<?=$bln_n[$bln];?>
        </option>
        <?php
	}
	?>
      </select>
      <select name="thn" id="thn">
       <?php
	for($thn=date('Y')-60;$thn<=date('Y')-15;$thn++){
	    $select_thn = ($thn == $thn_pisah) ? 'selected' : '';

		?>
        <option value="<?=$thn;?>" <?= $select_thn; ?>><?=$thn;?></option>
        <?php
	}
	?>
      </select>
      </td>
  
  
  </tr>
   <tr>
    <td>alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5"  >"<?= $alamat; ?>"</textarea></td>
  </tr>
   <tr>
    <td>telepon</td>
    <td>:</td>
    <td><input value="<?= $telp; ?>" type="text" name="telp" id="telp"></td>
    </td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="simpan" id="simpan" value="simpan">
      <input type="submit" name="batal" id="batal" value="reset">
      <br></br>
      <a href="<?=base_url();?>kasir/listkasir"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" value="kembali ke menu sebelumnya"></a></td>
  </tr>
</table>
</table>
</form>
